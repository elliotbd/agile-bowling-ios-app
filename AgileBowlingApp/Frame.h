//
//  BowlingFrame.h
//  AgileBowlingApp
//
//  Created by Elliott, Brian on 12/13/16.
//  Copyright © 2016 Focus on the Family. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Frame : NSObject

@property (nonatomic, strong) NSNumber *firstBall;
@property (nonatomic, strong) NSNumber *secondBall;
@property (nonatomic, strong) NSNumber *thirdBall;


- (id)initWithData:(NSNumber *)myFirstBall mySecondBall:(NSNumber *)mySecondBall;
- (id)initWithFullData:(NSNumber *)myFirstBall mySecondBall:(NSNumber *)mySecondBall myThirdBall:(NSNumber *)myThirdBall;

@end
