//
//  Game.h
//  AgileBowlingApp
//
//  Created by Elliott, Brian on 12/13/16.
//  Copyright © 2016 Focus on the Family. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Frame.h"

@interface Game : NSObject

@property (nonatomic, strong) NSMutableArray *frameList;
@property (nonatomic, strong) NSString *userName;

-(void)addFrame:(Frame *)aFrame;
-(int)getNumberOfFrames;
-(NSString *)getUserName;

@end
