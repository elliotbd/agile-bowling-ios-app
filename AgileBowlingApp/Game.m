//
//  Game.m
//  AgileBowlingApp
//
//  Created by Elliott, Brian on 12/13/16.
//  Copyright © 2016 Focus on the Family. All rights reserved.
//

#import "Game.h"

@implementation Game

@synthesize frameList;
@synthesize userName;

- (id)init {
    if (self = [super init]) {
        frameList = [NSMutableArray array];
        userName = @"Brian Elliott";
    }
    return self;
}

-(void)addFrame:(Frame *)aFrame {
    [frameList addObject:aFrame];
}

-(int)getNumberOfFrames {
    //return 0;
    return (int) [frameList count];
}

-(NSString *)getUserName {
    return userName;
}

@end
