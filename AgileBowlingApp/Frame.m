//
//  BowlingFrame.m
//  AgileBowlingApp
//
//  Created by Elliott, Brian on 12/13/16.
//  Copyright © 2016 Focus on the Family. All rights reserved.
//

#import "Frame.h"

@implementation Frame

@synthesize firstBall;
@synthesize secondBall;
@synthesize thirdBall;

- (id)initWithData:(NSNumber *)myFirstBall mySecondBall:(NSNumber *)mySecondBall {
    
    if (self = [super init]) {
        firstBall = myFirstBall;
        secondBall = mySecondBall;
        thirdBall = [NSNumber numberWithInt:0];
    }
    return self;
}

- (id)initWithFullData:(NSNumber *)myFirstBall mySecondBall:(NSNumber *)mySecondBall myThirdBall:(NSNumber *)myThirdBall {
    
    if (self = [super init]) {
        firstBall = myFirstBall;
        secondBall = mySecondBall;
        thirdBall = myThirdBall;
    }
    return self;
}

@end
