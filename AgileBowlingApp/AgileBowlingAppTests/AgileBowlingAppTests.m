//
//  AgileBowlingAppTests.m
//  AgileBowlingAppTests
//
//  Created by Elliott, Brian on 12/12/16.
//  Copyright © 2016 Focus on the Family. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ViewController.h"
#import "Game.h"
#import "Frame.h"
#import "BowlingStats.h"
#import <OCMock/OCMock.h>

//typedef struct frameCoupleStruct
//{
//    int first;
//    int second;
//} frameCoupleStruct;

@interface AgileBowlingAppTests : XCTestCase

@property (nonatomic) ViewController *vcToTest;
@property (nonatomic) Game *game;
@property (nonatomic) NSArray *frameCouples;
@property (nonatomic) NSArray *arrayOfThrows;

@end

@implementation AgileBowlingAppTests

@synthesize vcToTest;
@synthesize game;
@synthesize arrayOfThrows;

- (void)setUp {
    [super setUp];
    vcToTest = [[ViewController alloc] init];
    game = [[Game alloc] init];

    NSLog(@"setUp");
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    NSLog(@"tearDown");
}

- (void)testMock {
        
    id mockGameToTest = [OCMockObject partialMockForObject:game];
    
    [[[mockGameToTest stub] andReturn:@"Maxwell Smart"] getUserName];
    

    XCTAssertEqualObjects([mockGameToTest getUserName],
                          @"Maxwell Smart",
                          @"Proper name should be Maxwell Smart");

}

- (void)testCalcFirstFrame
{
    NSLog(@"testAddFrame");
    
    // one
    NSNumber *first = [NSNumber numberWithInt:1];
    NSNumber *second = [NSNumber numberWithInt:1];
    Frame *frame = [[Frame alloc] initWithData:first mySecondBall:second];
    [game addFrame:frame];
    
    BowlingStats *stats = [[BowlingStats alloc] init];
    [stats setGame:game];
    
    int firstFrameScore = [stats getFirstFrameScore];
    NSLog(@"testCalcFirstFrame .... firstFrameScore: %d", firstFrameScore);

    XCTAssertEqual((NSUInteger)firstFrameScore, (NSUInteger)2, @"First frame score wrong.");
}

- (void)testAddGame
{
    NSLog(@"testAddGame");
    
    //NSArray *firstBalls = @[@1, @2, @3, @4, @5, @6];
    //NSArray *secondBalls = @[@1, @2, @3, @4, @5, @6];
    
    // one
    NSNumber *first = [NSNumber numberWithInt:1];
    NSNumber *second = [NSNumber numberWithInt:1];
    Frame *frame = [[Frame alloc] initWithData:first mySecondBall:second];
    [game addFrame:frame];
    
    // two
    first = [NSNumber numberWithInt:1];
    second = [NSNumber numberWithInt:1];
    frame = [[Frame alloc] initWithData:first mySecondBall:second];
    [game addFrame:frame];
    
    // three
    first = [NSNumber numberWithInt:1];
    second = [NSNumber numberWithInt:1];
    frame = [[Frame alloc] initWithData:first mySecondBall:second];
    [game addFrame:frame];
    
    // four
    first = [NSNumber numberWithInt:1];
    second = [NSNumber numberWithInt:1];
    frame = [[Frame alloc] initWithData:first mySecondBall:second];
    [game addFrame:frame];
    
    // five
    first = [NSNumber numberWithInt:1];
    second = [NSNumber numberWithInt:1];
    frame = [[Frame alloc] initWithData:first mySecondBall:second];
    [game addFrame:frame];
    
    // six
    first = [NSNumber numberWithInt:1];
    second = [NSNumber numberWithInt:1];
    frame = [[Frame alloc] initWithData:first mySecondBall:second];
    [game addFrame:frame];
    
    // seven
    first = [NSNumber numberWithInt:1];
    second = [NSNumber numberWithInt:1];
    frame = [[Frame alloc] initWithData:first mySecondBall:second];
    [game addFrame:frame];
    
    // eight
    first = [NSNumber numberWithInt:1];
    second = [NSNumber numberWithInt:1];
    frame = [[Frame alloc] initWithData:first mySecondBall:second];
    [game addFrame:frame];
    
    // nine
    first = [NSNumber numberWithInt:1];
    second = [NSNumber numberWithInt:1];
    frame = [[Frame alloc] initWithData:first mySecondBall:second];
    [game addFrame:frame];
    
    // ten
    first = [NSNumber numberWithInt:1];
    second = [NSNumber numberWithInt:1];
    frame = [[Frame alloc] initWithData:first mySecondBall:second];
    [game addFrame:frame];
    
    int numberOfFrames = [game getNumberOfFrames];
    XCTAssertEqual((NSUInteger)numberOfFrames, (NSUInteger)10, @"Number of frames in game wrong.");
}

- (void)testAddFrame
{
    NSLog(@"testAddFrame");
    Frame *frame = [[Frame alloc] init];
    [game addFrame:frame];
    int numberOfFrames = [game getNumberOfFrames];
    XCTAssertEqual((NSUInteger)numberOfFrames, (NSUInteger)1, @"Number of frames array size wrong.");
}

- (void)testHelloWorld
{
    NSLog(@"testHelloWorld");
    NSString *helloWorldString = [self.vcToTest getHelloWorldString];
    XCTAssertEqualObjects(helloWorldString, @"Hello Word", @"Hello World string does not match");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

- (void)testReverseString
{
    NSLog(@"testReverseString");
    NSString *originalString = @"himynameisandy";
    NSString *reversedString = [self.vcToTest reverseString:originalString];
    NSString *expectedReversedString = @"ydnasiemanymih";
    XCTAssertEqualObjects(expectedReversedString, reversedString, @"The reversed string did not match the expected reverse");
}

@end
