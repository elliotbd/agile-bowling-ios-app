//
//  ViewController.m
//  AgileBowlingApp
//
//  Created by Elliott, Brian on 12/12/16.
//  Copyright © 2016 Focus on the Family. All rights reserved.
//

#import "ViewController.h"
#import "Constants.h"
#import "Game.h"
#import "Frame.h"
#import "BowlingStats.h"

@interface ViewController ()
- (IBAction)resetButtonPicked:(id)sender;

@property (nonatomic, strong) Game *game;
@property (nonatomic, strong) NSNumber *throwNumberInFrame;
@property (nonatomic, strong) NSNumber *currentFirstRoll;
@property (nonatomic, strong) NSNumber *currentSecondRoll;
@property (nonatomic, strong) NSNumber *currentThirdRoll;
@property (nonatomic, strong) NSNumber *currentNumRollsPerFrame;
@property (weak, nonatomic) IBOutlet UILabel *frameNumber;
@property (weak, nonatomic) NSNumber *frameNumberCount;
@property (nonatomic, strong) NSMutableDictionary *frameRollOutlets;
@property (weak, nonatomic) IBOutlet UIButton *rollButton;
@property (weak, nonatomic) IBOutlet UIButton *spareButton;
@property (weak, nonatomic) IBOutlet UIButton *strikeButton;
- (IBAction)getScoresPicked:(id)sender;

- (IBAction)getUserData:(id)sender;
- (IBAction)rollPicked:(id)sender;
- (IBAction)sparePicked:(id)sender;
- (IBAction)stikePicked:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *inputRollNumber;
@property (weak, nonatomic) IBOutlet UIButton *resetRollsPicked;

@property (weak, nonatomic) IBOutlet UILabel *rowOneFirstBallLabel;
@property (weak, nonatomic) IBOutlet UILabel *rowOneSecondBallLabel;
@property (weak, nonatomic) IBOutlet UILabel *rowTwoFirstBallLabel;
@property (weak, nonatomic) IBOutlet UILabel *rowTwoSecondBallLabel;
@property (weak, nonatomic) IBOutlet UILabel *thirdRowFirstBallLabel;
@property (weak, nonatomic) IBOutlet UILabel *thirdRowSecondBallLabel;
@property (weak, nonatomic) IBOutlet UILabel *forthRowFirstBallLabel;
@property (weak, nonatomic) IBOutlet UILabel *forthRowSecondBallLabel;
@property (weak, nonatomic) IBOutlet UILabel *fifthRowFirstBallLabel;
@property (weak, nonatomic) IBOutlet UILabel *fifthRowSecondBallLabel;
@property (weak, nonatomic) IBOutlet UILabel *sixthRowFirstBallLabel;
@property (weak, nonatomic) IBOutlet UILabel *sixthRowSecondBallLabel;
@property (weak, nonatomic) IBOutlet UILabel *seventhRowFirstBallLabel;
@property (weak, nonatomic) IBOutlet UILabel *seventhRowSecondBallLabel;
@property (weak, nonatomic) IBOutlet UILabel *eightRowFirstBallLabel;
@property (weak, nonatomic) IBOutlet UILabel *eigthRowSecondBallLabel;
@property (weak, nonatomic) IBOutlet UILabel *ninthRowFirstBallLabel;
@property (weak, nonatomic) IBOutlet UILabel *ninthRowSecondBallLabel;
@property (weak, nonatomic) IBOutlet UILabel *tenthRowFirstBallLabel;
@property (weak, nonatomic) IBOutlet UILabel *tenthRowSecondBallLabel;
@property (weak, nonatomic) IBOutlet UILabel *tenthRowThirdBallLabel;

@end

@implementation ViewController

@synthesize game;
@synthesize currentFirstRoll;
@synthesize currentSecondRoll;
@synthesize currentThirdRoll;
@synthesize throwNumberInFrame;
@synthesize frameRollOutlets;
@synthesize currentNumRollsPerFrame;

@synthesize frameNumberCount;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    game = [[Game alloc] init];
    frameNumberCount = [NSNumber numberWithInt:1];
    currentFirstRoll = [NSNumber numberWithInt:-1];
    currentSecondRoll = [NSNumber numberWithInt:-1];
    currentThirdRoll  = [NSNumber numberWithInt:-1];
    throwNumberInFrame  = [NSNumber numberWithInt:1];
    currentNumRollsPerFrame = [NSNumber numberWithInt:1];
    
    frameRollOutlets = [[NSMutableDictionary alloc] init];
    
    // load frame roll labels to aid in writing to labels
    [frameRollOutlets setObject:self.rowOneFirstBallLabel  forKey:@"1-1"];
    [frameRollOutlets setObject:self.rowOneSecondBallLabel forKey:@"1-2"];
    [frameRollOutlets setObject:self.rowTwoFirstBallLabel  forKey:@"2-1"];
    [frameRollOutlets setObject:self.rowTwoSecondBallLabel forKey:@"2-2"];
    [frameRollOutlets setObject:self.thirdRowFirstBallLabel forKey:@"3-1"];
    [frameRollOutlets setObject:self.thirdRowSecondBallLabel forKey:@"3-2"];
    [frameRollOutlets setObject:self.forthRowFirstBallLabel forKey:@"4-1"];
    [frameRollOutlets setObject:self.forthRowSecondBallLabel forKey:@"4-2"];
    [frameRollOutlets setObject:self.fifthRowFirstBallLabel forKey:@"5-1"];
    [frameRollOutlets setObject:self.fifthRowSecondBallLabel forKey:@"5-2"];
    [frameRollOutlets setObject:self.sixthRowFirstBallLabel forKey:@"6-1"];
    [frameRollOutlets setObject:self.sixthRowSecondBallLabel forKey:@"6-2"];
    [frameRollOutlets setObject:self.seventhRowFirstBallLabel forKey:@"7-1"];
    [frameRollOutlets setObject:self.seventhRowSecondBallLabel forKey:@"7-2"];
    [frameRollOutlets setObject:self.eightRowFirstBallLabel forKey:@"8-1"];
    [frameRollOutlets setObject:self.eigthRowSecondBallLabel forKey:@"8-2"];
    [frameRollOutlets setObject:self.ninthRowFirstBallLabel forKey:@"9-1"];
    [frameRollOutlets setObject:self.ninthRowSecondBallLabel forKey:@"9-2"];
    [frameRollOutlets setObject:self.tenthRowFirstBallLabel forKey:@"10-1"];
    [frameRollOutlets setObject:self.tenthRowSecondBallLabel forKey:@"10-2"];
    [frameRollOutlets setObject:self.tenthRowThirdBallLabel forKey:@"10-3"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateFrameLabels:(int)roll numberPins:(int)numberPins {
    
    int currentFrameNumber = [frameNumberCount intValue];
    NSString *indexString = [NSString stringWithFormat:@"%d-%d", currentFrameNumber, roll];
    UILabel *targetLabel = [frameRollOutlets objectForKey:indexString];
    targetLabel.text = [NSString stringWithFormat:@"%d", numberPins];
    
}


-(NSString *)compareLastFramePins:(int)firstBall secondBall:(int)secondBall thirdBall:(int)thirdBall {
    
    NSString *returnString = @"";
    
    if (firstBall == 10) {
        returnString = @"X";
    }
    
    if (secondBall == 10) {
        returnString = [NSString stringWithFormat:@"%@X",returnString];
    }
    
    if (thirdBall == 10) {
        returnString = [NSString stringWithFormat:@"%@X",returnString];
    }
    
    if (firstBall + secondBall == 10) {
        returnString = [NSString stringWithFormat:@"%@/", returnString];
    }
    
    if (secondBall + thirdBall == 10) {
        returnString = [NSString stringWithFormat:@"%@/", returnString];
    }
    
    if (firstBall + secondBall < 10) {
        
        if (firstBall == 0) {
            returnString = [NSString stringWithFormat:@"%@-",returnString];
        }
        
        if (secondBall == 0) {
            returnString = [NSString stringWithFormat:@"%@-",returnString];
        }
        
        if (firstBall > 0 && secondBall > 0) {
            returnString = [NSString stringWithFormat:@"%d%d", firstBall, secondBall];
        }
    }
    
    if (thirdBall < 10) {
        if (thirdBall == 0) {
            returnString = [NSString stringWithFormat:@"%@-",returnString];
        }
        else {
            returnString = [NSString stringWithFormat:@"%@%d", returnString, thirdBall];
        }
    }
    
    return returnString;
}
 

-(NSString *)comparePins:(int)firstBall secondBall:(int)secondBall {
    
    NSString *returnString = @"";
    
    if (firstBall == 10) {
        returnString = @"X";
    }
    else if (firstBall + secondBall == 10) {
        returnString = @"/";
    }
    else if (firstBall == 0) {
        returnString = @"-";
    }
    else if (secondBall == 0) {
        returnString = @"-";
    }
    else {
        returnString = [NSString stringWithFormat:@"%d%d", firstBall, secondBall];
    }
    
    return returnString;
}

- (IBAction)getScoresPicked:(id)sender {
    
    NSString *gameString = @"";
    
    for (int currentFrameNumber = 1; currentFrameNumber <= 10; currentFrameNumber++)
    {

        NSString *firstIndexString = [NSString stringWithFormat:@"%d-%d", currentFrameNumber, 1];
        UILabel *firstLabel = [frameRollOutlets objectForKey:firstIndexString];
        int firstBallValue = [firstLabel.text intValue];
        
        NSString *secondIndexString = [NSString stringWithFormat:@"%d-%d", currentFrameNumber, 2];
        UILabel *secondLabel = [frameRollOutlets objectForKey:secondIndexString];
        int secondBallValue = [secondLabel.text intValue];
        
        if (currentFrameNumber == 10) {
            NSString *thirdIndexString = [NSString stringWithFormat:@"%d-%d", currentFrameNumber, 3];
            UILabel *thirdLabel = [frameRollOutlets objectForKey:thirdIndexString];
            int thirdBallValue = [thirdLabel.text intValue];
            
            NSString *digestedString = [self compareLastFramePins:firstBallValue secondBall:secondBallValue thirdBall:thirdBallValue];
            gameString = [NSString stringWithFormat:@"%@ %@", gameString, digestedString];
        }
        else {
            NSString *digestedString = [self comparePins:firstBallValue secondBall:secondBallValue];
            gameString = [NSString stringWithFormat:@"%@ %@", gameString, digestedString];
        }
        
    }
    
    NSLog(@"getScoresPicked gameString: %@", gameString);
    [self getServerScore:gameString];
    
}

-(void)getServerScore:(NSString *)gameString {
    
    BowlingStats *stats = [[BowlingStats alloc] init];
    int gameScore = [stats getGameScore:gameString];
    
    //NSString *alertString = [NSString stringWithFormat:@"Score: %d", gameScore];
    
   /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congratulations"
                                                    message:alertString
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    */
}

- (IBAction)getUserData:(id)sender {
}

-(void) setRollInFrame:(int)rollCount {
    
    if ([throwNumberInFrame intValue] == 1) {
        currentFirstRoll = [NSNumber numberWithInt:rollCount];
    }
    else if ([throwNumberInFrame intValue] == 2) {
        currentSecondRoll = [NSNumber numberWithInt:rollCount];
        
        Frame *frame = [[Frame alloc] initWithData:currentFirstRoll mySecondBall:currentSecondRoll];
        [game addFrame:frame];
        
        //[self updateFrameLabels:frame];
        self.frameNumber.text = [NSString stringWithFormat:@"Frame: %d", [frameNumberCount intValue]];
        
        // Reset for next frame
        currentFirstRoll = [NSNumber numberWithInt:-1];
        currentSecondRoll = [NSNumber numberWithInt:-1];
        currentThirdRoll  = [NSNumber numberWithInt:-1];
    }
    else if ([frameNumberCount intValue] == MAX_FRAMES && [throwNumberInFrame intValue] == 3) {
        currentThirdRoll  = [NSNumber numberWithInt:rollCount];
        
        Frame *frame = [[Frame alloc] initWithFullData:currentFirstRoll mySecondBall:currentSecondRoll myThirdBall:currentThirdRoll];
        [game addFrame:frame];
        
        // Reset for next frame
        currentFirstRoll = [NSNumber numberWithInt:-1];
        currentSecondRoll = [NSNumber numberWithInt:-1];
        currentThirdRoll  = [NSNumber numberWithInt:-1];
    }
    

    
 }

/*
-(Frame *)getNewFrame {
    
    currentFirstRoll = [NSNumber numberWithInt:-1];
    currentSecondRoll = [NSNumber numberWithInt:-1];
    currentThirdRoll  = [NSNumber numberWithInt:-1];
    
    NSNumber *first = [NSNumber numberWithInt:1];
    NSNumber *second = [NSNumber numberWithInt:1];
    Frame *frame = [[Frame alloc] initWithData:first mySecondBall:second];
    [game addFrame:frame];
}

-(int)rollsForThisFrame {
    
}
 */

-(void) stopBowling {
    [self.rollButton setUserInteractionEnabled:NO];
    [self.strikeButton setUserInteractionEnabled:NO];
    [self.spareButton setUserInteractionEnabled:NO];
}

-(void) resetTargetPointer:(int)rollsForThisFrame {
    
    // increment and pointer for next button pick -- setting roll count  and frame
    if (rollsForThisFrame == 2 && [frameNumberCount intValue] < MAX_FRAMES) {
        [self incrementFrameCount];
        currentNumRollsPerFrame = [NSNumber numberWithInt:1];
    }
    else if (rollsForThisFrame == 3 && [frameNumberCount intValue] == MAX_FRAMES) {
        currentNumRollsPerFrame = [NSNumber numberWithInt:1];
        // stop!!
        [self stopBowling];
    }
    else {
        currentNumRollsPerFrame = [NSNumber numberWithInt:rollsForThisFrame+1];
    }
}

- (IBAction)rollPicked:(id)sender {
    
    int rollsForThisFrame = [currentNumRollsPerFrame intValue];

    NSString *numberPinsString = self.inputRollNumber.text;
    int numberPins = [numberPinsString intValue];

    [self updateFrameLabels:[currentNumRollsPerFrame intValue] numberPins:numberPins];
    
    [self resetTargetPointer:rollsForThisFrame];
}

- (IBAction)sparePicked:(id)sender {
    [self incrementFrameCount];
}

- (IBAction)stikePicked:(id)sender {
    [self incrementFrameCount];
}

-(void)incrementFrameCount {
    
    int current = [frameNumberCount intValue];
    if (current >= MAX_FRAMES) {
        frameNumberCount = [NSNumber numberWithInt:MAX_FRAMES];
    }
    else {
       frameNumberCount = [NSNumber numberWithInt:current + 1];
    }
    
    
    self.frameNumber.text = [NSString stringWithFormat:@"Frame: %d", [frameNumberCount intValue]];
}

- (NSString *)reverseString:(NSString *)stringToReverse
{
    NSMutableString *reversedString = [NSMutableString stringWithCapacity:[stringToReverse length]];
    
    [stringToReverse enumerateSubstringsInRange:NSMakeRange(0,[stringToReverse length])
                                        options:(NSStringEnumerationReverse | NSStringEnumerationByComposedCharacterSequences)
                                     usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
                                         [reversedString appendString:substring];
                                     }];
    return reversedString;
}

- (NSString *)getHelloWorldString {
    return @"Hello Word";
}

- (IBAction)resetButtonPicked:(id)sender {
    
    for (id key in frameRollOutlets) {
        UILabel *label = [frameRollOutlets objectForKey:key];
        label.text = @"*";
    }
    
    frameNumberCount = [NSNumber numberWithInt:1];
    currentFirstRoll = [NSNumber numberWithInt:-1];
    currentSecondRoll = [NSNumber numberWithInt:-1];
    currentThirdRoll  = [NSNumber numberWithInt:-1];
    throwNumberInFrame  = [NSNumber numberWithInt:1];
    currentNumRollsPerFrame = [NSNumber numberWithInt:1];
    
    [self.rollButton setUserInteractionEnabled:YES];
    [self.strikeButton setUserInteractionEnabled:YES];
    [self.spareButton setUserInteractionEnabled:YES];
    
    self.frameNumber.text = [NSString stringWithFormat:@"Frame: %d", [frameNumberCount intValue]];
}
@end
