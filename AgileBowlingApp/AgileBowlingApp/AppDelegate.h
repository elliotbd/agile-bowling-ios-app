//
//  AppDelegate.h
//  AgileBowlingApp
//
//  Created by Elliott, Brian on 12/12/16.
//  Copyright © 2016 Focus on the Family. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

