//
//  ViewController.h
//  AgileBowlingApp
//
//  Created by Elliott, Brian on 12/12/16.
//  Copyright © 2016 Focus on the Family. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (NSString *)reverseString:(NSString *)stringToReverse;
- (NSString *)getHelloWorldString;

@end

