//
//  BowlingStats.h
//  AgileBowlingApp
//
//  Created by Elliott, Brian on 12/13/16.
//  Copyright © 2016 Focus on the Family. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Game.h"
#import "Frame.h"

@interface BowlingStats : NSObject

@property (nonatomic, strong)Game *game;

-(int)getFirstFrameScore;
-(void)setGame:(Game *)myGame;
-(int)getGameScore:gameString;

@end
