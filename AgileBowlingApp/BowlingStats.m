//
//  BowlingStats.m
//  AgileBowlingApp
//
//  Created by Elliott, Brian on 12/13/16.
//  Copyright © 2016 Focus on the Family. All rights reserved.
//

#import "BowlingStats.h"
#import "AFHTTPRequestOperationManager.h"


@implementation BowlingStats

@synthesize game;

-(void)setGame:(Game *)myGame {
    game = myGame;
}

-(int)getFirstFrameScore {
    
    Frame *first = [[game frameList] objectAtIndex:0];
    //return 0;
    return [first.firstBall intValue] + [first.secondBall intValue];
}

-(int)getGameScore:gameString {
    
    NSString *URLString = @"https://gu937tqj6i.execute-api.us-east-1.amazonaws.com/prod";
    //NSDictionary *parameters = @{@"game": @"13 X 32 3/ 18 9- 81 9- 11 72"};
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    parameters[@"game"] = gameString;
    
 //   [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:URLString parameters:parameters error:nil];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // in my case, I'm in prototype mode, I own the network being used currently,
    // so I can use a self generated cert key, and the following line allows me to use that
    manager.securityPolicy.allowInvalidCertificates = YES;
    // Make sure we a JSON serialization policy, not sure what the default is
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    // No matter the serializer, they all inherit a battery of header setting APIs
    // Here we do Basic Auth, never do this outside of HTTPS

    // Now we can just PUT it to our target URL (note the https).
    // This will return immediately, when the transaction has finished,
    // one of either the success or failure blocks will fire
    
 //   [manager
   //  POST: URLString
   //  parameters: parameters
   //  success:^(AFHTTPRequestOperation *operation, id responseObject){
    
    int returnValue = 0;
    
    [manager
     POST: URLString
     parameters: parameters
     success:^(AFHTTPRequestOperation *operation, id responseObject){
         
            NSLog(@"Submit response data: %@", responseObject);
         
         NSString *responseString = [responseObject objectForKey:@"score"];
         NSLog(@"nate response string: %@", responseString);
         
         NSString *alertString = [NSString stringWithFormat:@"Score: %d", [responseString intValue]];
         
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congratulations"
                                                         message:alertString
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
         [alert show];
         
         //returnValue = [responseString intValue];
         
 //           NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
 //        NSJSONReadingOptions JSONReadingOptions;
  //       NSData *JSONData = [responseObject dataUsingEncoding:NSUTF8StringEncoding];
  //       id responseJSON = [NSJSONSerialization JSONObjectWithData:JSONData options:JSONReadingOptions error:nil];
         
  //          int score = [[responseJSON objectForKey:@"score"] intValue];
  //          NSLog(@"nate returning score: %d", score);
         
         /*
         NSDictionary *json_string = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
         
         //NSString *json_string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         NSDictionary *json_dict = (NSDictionary *)json_string;
          */
         /*
         NSError *e = nil;
         NSMutableArray *jsonArray = [NSJSONSerialization JSONObjectWithData: jsonData options: NSJSONReadingMutableContainers error: &e];
         
         if (!jsonArray) {
             NSLog(@"Error parsing JSON: %@", e);
         } else {
             for(NSDictionary *item in jsonArray) {
                 NSLog(@"Item: %@", item);
             }
         }
          */
         
         //NSLog(@"json_dict\n%@",json_dict);
         //NSLog(@"json_string\n%@",json_string);
         
     } // success callback block
     


     failure:^(AFHTTPRequestOperation *operation, NSError *error){
         NSLog(@"Error: %@", error);} // failure callback block
     ];
    
    return 0;
}

@end
